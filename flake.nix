{
  description = "Sample Nix node build";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    nix-filter.url = "github:numtide/nix-filter";
  };
  outputs = { self, nixpkgs, flake-utils, nix-filter, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        nodejs = pkgs.nodejs;
        nodeDependencies = (pkgs.callPackage ./default.nix { }).nodeDependencies;
        filter = nix-filter.lib;
        app = pkgs.stdenv.mkDerivation {
          name = "example-node";
          version = "0.1.0";
          src = filter {
            root = ./.;
            exclude = [
              "./node_modules"
              "./result"
            ];
          };
          buildInputs = [ nodejs ];
          buildPhase = ''
            runHook preBuild

            ln -s ${nodeDependencies}/lib/node_modules ./node_modules
            export PATH="${nodeDependencies}/bin:$PATH"
            npm run build

            runHook postBuild
          '';
          installPhase = ''
            runHook preInstall

            mkdir -p $out/bin

            cp package.json $out/package.json
            cp -r dist $out/dist
            ln -s ${nodeDependencies}/lib/node_modules $out/node_modules

            cp dist/index.js $out/bin/main
            chmod +x $out/bin/main

            runHook postInstall
          '';
        };
      in
      with pkgs;{
        packages.default = app;
        devShell = mkShell { buildInputs = [ nodejs node2nix ]; };
        packages.docker = dockerTools.buildImage {
          name = app.name;
          config = {
            Cmd = [ "/bin/main" ];
          };
          copyToRoot = buildEnv {
            name = "root";
            paths = [ app ];
            pathsToLink = [ "/bin" "/lib" ];
          };
          # This ensures symlinks to directories are preserved in the image
          keepContentsDirlinks = true;
        };
      });
}
